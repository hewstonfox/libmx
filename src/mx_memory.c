#include "../inc/libmx.h"

void *mx_memset(void *b, int c, size_t len) {
    for (size_t i = 0; i < len; i++) ((uchar_t *) b)[i] = (uchar_t) c;
    return b;
}

void *mx_memcpy(void *restrictdst, const void *restrictsrc, size_t n) {
    for (size_t i = 0; i < n; i++)
        ((uchar_t *) restrictdst)[i] = ((uchar_t *) restrictsrc)[i];
    return restrictdst;
}

void *mx_memccpy(void *restrictdst, const void *restrictsrc, int c, size_t n) {
    for (size_t i = 0; i < n; i++) {
        ((uchar_t *) restrictdst)[i] = ((uchar_t *) restrictsrc)[i];
        if (((uchar_t *) restrictdst)[i] == c)
            return (uchar_t *) restrictdst + i + 1;
    }
    return NULL;
}

int mx_memcmp(const void *s1, const void *s2, size_t n) {
    if (n == 0) return 0;
    const uchar_t *_s1 = (const uchar_t *) s1;
    const uchar_t *_s2 = (const uchar_t *) s2;
    for (size_t i = 0; *_s1 == *_s2 && i != n; i++) {
        _s1++;
        _s2++;
    }
    return *_s1 - *_s2;
}

void *mx_memchr(const void *s, int c, size_t n) {
    for (size_t i = 0; i < n; i++)
        if (((uchar_t *) s)[i] == c) return (uchar_t *) s + i;
    return NULL;
}

void *mx_memrchr(const void *s, int c, size_t n) {
    for (int i = n - 1; i >= 0; i--)
        if (((uchar_t *) s)[i] == c) return (uchar_t *) s + i;
    return NULL;
}

void *mx_memmem(const void *big, size_t big_len, const void *little,
                size_t little_len) {
    if (big_len < little_len || !big_len || !little_len) return NULL;
    uchar_t *_big = (uchar_t *) big;
    for(size_t i = 0; i < big_len; i++) {
        if (mx_memcmp(_big, little, little_len - 1) == 0) return _big;
        _big++;
    }
    return NULL;
}

void *mx_memmove(void *dst, const void *src, size_t len) {
    void *temp = malloc(len);
    temp = mx_memcpy(temp, src, len);
    dst = mx_memcpy(dst, temp, len);
    free(temp);
    temp = NULL;
    return dst;
}

void *mx_realloc(void *ptr, size_t size) {
    if (!ptr) return malloc(size);
    size_t cur_size = malloc_size(ptr);
    if (size <= cur_size) return ptr;
    void *new_ptr = malloc(size);
    mx_memcpy(new_ptr, ptr, cur_size);
    free(ptr);
    return new_ptr;
}

void *mx_memjoin_mutation(void **dst, size_t *dst_len, void *src, size_t src_len) {
    size_t _dst_len = *dst_len;
    void *_dst = *dst;
    if (!_dst)
        _dst_len = 0;
    if (!src)
        src_len = 0;
    if (!(_dst_len + src_len))
        return NULL;

    void *buff = malloc(_dst_len + src_len);

    if (_dst_len)
        mx_memcpy(buff, _dst, _dst_len);
    if (src_len)
        mx_memcpy((unsigned char *)buff + _dst_len, src, src_len);
    if (_dst)
        free(_dst);
    *dst = buff;
    *dst_len = _dst_len + src_len;
    return buff;
}
