#include "../inc/libmx.h"

int mx_strlen(const char *s) {
    int count = 0;
    while (s[count])
        count++;
    return count;
}

void mx_swap_char(char *s1, char *s2) {
    char temp = *s1;
    *s1 = *s2;
    *s2 = temp;
}

void mx_str_reverse(char *s) {
    int length = mx_strlen(s) - 1;
    int half_length = length / 2 + (length & 1 ? 1 : 0);
    for (int i = 0; i < half_length; i++)
        mx_swap_char(&s[i], &s[length - i]);
}

void mx_strdel(char **str) {
    free(*str);
    *str = NULL;
}

void mx_del_strarr(char ***arr) {
    if (!arr || !*arr || !**arr)
        return;
    for (int i = 0; (*arr)[i] != NULL; i++)
        free((*arr)[i]);
    free(*arr);
    *arr = NULL;
}

char *mx_strcpy(char *dst, const char *src) {
    char *orig = dst;
    while (*src)
        *dst++ = *src++;
    *dst = *src;
    return orig;
}

char *mx_strdup(const char *s1) {
    char *dup = mx_strnew(mx_strlen(s1));
    mx_strcpy(dup, s1);
    return dup;
}

char *mx_strndup(const char *s1, size_t n) {
    size_t len = (size_t)mx_strlen(s1);
    if (n < len)
        len = n;
    char *dup = mx_strnew(len);
    mx_strncpy(dup, s1, len);
    return dup;
}

char *mx_strncpy(char *dst, const char *src, int len) {
    int i = 0;
    while (src[i] && i != len) {
        dst[i] = src[i];
        i++;
    }
    if (i != len)
        dst[i] = src[i];
    return dst;
}

int mx_strcmp(const char *s1, const char *s2) {
    while (*s1 && *s1 == *s2) {
        s1++;
        s2++;
    }
    return *s1 - *s2;
}

int mx_strcmpi(const char *s1, const char *s2) {
    while (*s1 && mx_tolower(*s1) == mx_tolower(*s2)) {
        s1++;
        s2++;
    }
    return mx_tolower(*s1) - mx_tolower(*s2);
}

bool mx_streq(const char *s1, const char *s2) { return !mx_strcmp(s1, s2); }
bool mx_streqi(const char *s1, const char *s2) { return !mx_strcmpi(s1, s2); }

int mx_strncmp(const char *s1, const char *s2, int n) {
    while (n--) {
        if (*s1 != *s2)
            return *(const uchar_t *)s1 - *(const uchar_t *)s2;
        s1++;
        s2++;
    }
    return 0;
}

char *mx_strcat(char *restricts1, const char *restricts2) {
    for (char *i = restricts1 + mx_strlen(restricts1); *restricts2; i++) {
        *i = *restricts2;
        restricts2++;
    }
    return restricts1;
}

char *mx_strchr(const char *s, int c) {
    for (; *s; s++)
        if (*s == c)
            return (char *)s;
    return NULL;
}

char *mx_strstr(const char *haystack, const char *needle) {
    int len = mx_strlen(needle);
    while (*haystack) {
        if (!mx_strncmp(haystack, needle, len))
            return (char *)haystack;
        haystack++;
    }
    return NULL;
}

int mx_count_substr(const char *str, const char *sub) {
    int count = 0;
    if (str == NULL || sub == NULL)
        return -1;
    if (mx_strlen(str) < mx_strlen(sub))
        return 0;
    while ((str = mx_strstr(str, sub))) {
        count++;
        str++;
    }
    return count;
}

int mx_count_char(const char *str, char c) {
    int count = 0;
    while (*str)
        if (*str++ == c)
            count++;
    return count;
}

int mx_count_words(const char *str, char c) {
    while (*str == c)
        str++;
    if (!*str)
        return 0;
    int count = 0;
    int skip = 0;
    while (*str) {
        char tmp = *str++;
        if (tmp == c && skip)
            continue;
        if (tmp == c) {
            skip = 1;
            count++;
            continue;
        }
        skip = 0;
    }
    if (*(str - 1) != c)
        count++;
    return count;
}

char *mx_strnew(const int size) {
    char *str = (char *)malloc((size + 1) * sizeof(char));
    if (str == NULL)
        return NULL;
    for (int i = 0; i <= size; ++i)
        str[i] = '\0';
    return str;
}

char *mx_strtrim(const char *str) {
    if (!str)
        return NULL;
    while (mx_isspace(*str))
        str++;
    int len = mx_strlen(str);
    while (mx_isspace(str[len - 1]))
        len--;
    return mx_strndup(str, len);
}

char **mx_strsplit(const char *s, char c) {
    if (s == NULL)
        return NULL;
    char **words = (char **)malloc((mx_count_words(s, c) + 1) * sizeof(char *));
    int skip = 0;
    int char_count = 0;
    int word_idx = 0;
    while (*s == c)
        s++;
    while (*s) {
        if (*s++ == c) {
            if (!skip) {
                words[word_idx] = mx_strnew(char_count);
                mx_strncpy(words[word_idx], s - char_count - 1, char_count);
                word_idx++;
                skip = 1;
                char_count = 0;
            }
            continue;
        }
        skip = 0;
        char_count++;
        if (*s == '\0')
            words[word_idx++] =
                mx_strncpy(mx_strnew(char_count), s - char_count, char_count);
    }
    words[word_idx] = NULL;
    return words;
}

/**
 * @brief
 *
 * @param s str to split
 * @param c char to split str by
 * @param splits_count max count of splits, -1 to split without limit
 * @param skip_empty skip empty entries
 * @return char** NULL terminated array of strings
 */
char **mx_strsplit_extended(const char *s, char c, int splits_count, bool skip_empty) {
    if (s == NULL)
        return NULL;
    int len = (skip_empty ? mx_count_words(s, c) : mx_count_char(s, c) + 1) + 1;
    char **words = (char **)malloc(len * sizeof(char *));
    mx_memset(words, 0, len);
    int skip = 0;
    int char_count = 0;
    int word_idx = 0;
    while (*s == c && splits_count) {
        if (!skip_empty) {
            words[word_idx++] = mx_strdup("");
            splits_count--;
        }
        s++;
    }
    while (*s && splits_count) {
        if (*s++ == c) {
            if (!skip || !skip_empty) {
                words[word_idx] = mx_strnew(char_count);
                mx_strncpy(words[word_idx], s - char_count - 1, char_count);
                word_idx++;
                skip = 1;
                char_count = 0;
                splits_count--;
            }
            continue;
        }
        skip = 0;
        char_count++;
        if (*s == '\0')
            words[word_idx++] =
                mx_strncpy(mx_strnew(char_count), s - char_count, char_count);
    }
    if (!splits_count) {
        words[word_idx++] = mx_strdup(s);
    }
    words[word_idx] = NULL;
    return words;
}

char **mx_strdivide(const char *s, char c) {
    int divider = mx_get_char_index(s, c);
    string_t *res = (string_t *)(malloc(sizeof(string_t) * 3));
    res[2] = NULL;
    if (divider == -1) {
        res[0] = mx_strdup(s);
        res[1] = mx_strdup("");
    } else {
        res[0] = mx_strndup(s, divider);
        res[1] = mx_strdup(s + divider + 1);
    }
    return res;
}

char *mx_strjoin(const char *s1, const char *s2) {
    if (s1 == NULL && s2 == NULL)
        return NULL;
    if (s1 == NULL)
        return mx_strdup(s2);
    if (s2 == NULL)
        return mx_strdup(s1);
    char *str = mx_strnew(mx_strlen(s1) + mx_strlen(s2));
    mx_strcat(str, s1);
    mx_strcat(str, s2);
    return str;
}

char *mx_file_to_str(const char *file) {
    if (file == NULL)
        return NULL;
    int f = open(file, O_RDONLY);
    if (f < 0)
        return NULL;
    char buffer;
    int length = 0;
    while (read(f, &buffer, 1))
        length++;
    if (close(f) < 0)
        return NULL;
    if (length == 0)
        return mx_strdup("");
    f = open(file, O_RDONLY);
    if (f < 0)
        return NULL;
    char *str = mx_strnew(length);
    read(f, str, length);
    if (close(f) < 0)
        return NULL;
    return str;
}

int mx_get_char_index(const char *str, char c) {
    if (!str || !*str)
        return -2;
    int len = mx_strlen(str);
    for (int i = 0; i < len; i++)
        if (str[i] == c)
            return i;
    return -1;
}

int mx_get_substr_index(const char *str, const char *sub) {
    if (!str || !*str || !sub || !*sub)
        return -2;
    char *res_point = mx_strstr(str, sub);
    if (!res_point)
        return -1;
    return res_point - str;
}

char *mx_del_extra_spaces(const char *str) {
    if (!str)
        return NULL;
    char *trimmed = mx_strtrim(str);
    int pointer = 0;
    int current = 0;
    while (trimmed[pointer]) {
        trimmed[current++] = trimmed[pointer++];
        if (!mx_isspace(trimmed[pointer]))
            continue;
        while (mx_isspace(trimmed[pointer]))
            pointer++;
        trimmed[current++] = ' ';
    }
    trimmed[current] = '\0';
    char *result = mx_strncpy(mx_strnew(current), trimmed, current);
    mx_strdel(&trimmed);
    return result;
}

char *mx_replace_substr(const char *str, const char *sub, const char *replace) {
    char *result;
    char *ins;
    char *tmp;
    int len_front;
    int count;
    if (!str)
        return NULL;
    if (!sub || !replace)
        return mx_strdup(str);
    int len_rep = mx_strlen(sub);
    if (len_rep == 0)
        return mx_strdup(str);
    int len_with = mx_strlen(replace);
    ins = (char *)str;
    for (count = 0; (tmp = mx_strstr(ins, sub)); ++count)
        ins = tmp + len_rep;
    tmp = result =
        (char *)malloc(mx_strlen(str) + (len_with - len_rep) * count + 1);
    if (!result)
        return NULL;
    while (count--) {
        ins = mx_strstr(str, sub);
        len_front = ins - str;
        tmp = mx_strncpy(tmp, str, len_front) + len_front;
        tmp = mx_strcpy(tmp, replace) + len_with;
        str += len_front + len_rep;
    }
    mx_strcpy(tmp, str);
    return result;
}

int mx_read_line(char **lineptr, size_t buf_size, char delim, const int fd) {
    if (buf_size == 0)
        return -2;
    buf_size = 1;
    int res = 0;
    char *temp = *lineptr;
    *lineptr = NULL;
    char *buffer = mx_strnew(buf_size);
    int bytes;
    char *to_free;
    int index;
    while ((bytes = read(fd, buffer, buf_size)) > 0) {
        index = mx_get_char_index(buffer, delim);
        if (index >= 0)
            buffer[index] = '\0';
        to_free = *lineptr;
        *lineptr = mx_strjoin(*lineptr, buffer);
        mx_strdel(&to_free);
        if (index >= 0) {
            res += mx_strlen(buffer);
            break;
        }
        res += bytes;
    }
    mx_strdel(&buffer);
    if (res == 0) {
        *lineptr = temp;
        return bytes == -1 ? -2 : -1;
    }
    return res;
}

char **mx_dup_string_arr(const char **array, const int size) {
    char **dup_arr = malloc(sizeof(char *) * (size + 1));
    for (int i = 0; i < size; i++) {
        dup_arr[i] = mx_strdup(array[i]);
    }
    dup_arr[size] = NULL;
    return dup_arr;
}

void mx_del_str_arr(char **array, int size) {
    for (int i = 0; i < size; i++) {
        free(array[i]);
        array[i] = NULL;
    }
    free(array);
    array = NULL;
}
