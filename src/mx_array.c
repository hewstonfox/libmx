#include "../inc/libmx.h"

bool mx_some(void **arr, size_t len, is_valid_t cmp) {
    for (size_t i = 0; i < len; i++)
        if (cmp(arr[i])) return true;
    return false;
}

bool mx_every(void **arr, size_t len, is_valid_t cmp) {
    for (size_t i = 0; i < len; i++)
        if (!cmp(arr[i])) return false;
    return true;
}

bool mx_some_is(void **arr, size_t len, cmp_t cmp, void *predicate) {
    for (size_t i = 0; i < len; i++)
        if (cmp(arr[i], predicate)) return true;
    return false;
}

bool mx_every_is(void **arr, size_t len, cmp_t cmp, void *predicate) {
    for (size_t i = 0; i < len; i++)
        if (!cmp(arr[i], predicate)) return false;
    return true;
}

void *mx_find(void **arr, size_t len, cmp_t cmp, void *predicate) {
    for (size_t i = 0; i < len; i++)
        if (cmp(arr[i], predicate)) return arr[i];
    return NULL;
}

size_t mx_find_index(void **arr, size_t len, cmp_t cmp, void *predicate) {
    for (size_t i = 0; i < len; i++)
        if (cmp(arr[i], predicate)) return i;
    return -1;
}

void mx_sort(void **arr, size_t len, cmp_t cmp) {
    for (size_t i = 1; i < len; i++) {
        void *tmp = arr[i];
        int j = i - 1;
        while (j >= 0 && !cmp(tmp, arr[j])) {
            arr[j + 1] = arr[j];
            j--;
        }
        arr[j + 1] = tmp;
    }
}
