#include "../inc/libmx.h"

t_list *mx_create_node(void *data) {
    t_list *node = (t_list *) malloc(sizeof(t_list));
    node->data = data;
    node->next = NULL;
    return node;
}

void mx_push_front(t_list **list, void *data) {
    if (!*list) {
        *list = mx_create_node(data);
        return;
    }
    t_list *node = mx_create_node(data);
    node->next = *list;
    *list = node;
}

void mx_push_back(t_list **list, void *data) {
    if (!*list) {
        *list = mx_create_node(data);
        return;
    }
    t_list *cur = *list;
    while (cur->next) cur = cur->next;
    cur->next = mx_create_node(data);
}

void mx_pop_front(t_list **head) {
    if (head == NULL || *head == NULL) return;
    if ((*head)->next == NULL) {
        free(*head);
        *head = NULL;
        return;
    }
    t_list *temp = (*head)->next;
    free(*head);
    *head = temp;
}

void mx_pop_back(t_list **head) {
    if (head == NULL || *head == NULL) return;
    if ((*head)->next == NULL) {
        free(*head);
        *head = NULL;
        return;
    }
    t_list *temp = *head;
    while (temp && temp->next->next) temp = temp->next;
    free(temp->next);
    temp->next = NULL;
}

int mx_list_size(t_list *list) {
    int count = 0;
    for (; list; list = list->next) count++;
    return count;
}

t_list *mx_sort_list(t_list *lst, cmp_t cmp) {
    if (cmp == NULL || lst == NULL) return lst;
    for (t_list *i = lst; i; i = i->next)
        for (t_list *j = lst; j->next; j = j->next)
            if (cmp(j->data, j->next->data)) {
                void *data = j->data;
                j->data = j->next->data;
                j->next->data = data;
            }
    return lst;
}
